import React from "react";

function NumberInput({
  value,
  onChange,
  style = {}
}: {
  value: number;
  onChange: (value: number) => void;
  style?: React.CSSProperties;
}) {
  return (
    <input
      style={{
        background: "none",
        border: "none",
        width: 60,
        borderBottom: "1px solid #001733",
        margin: 8,
        textAlign: "center",
        ...style
      }}
      onChange={e => {
        onChange(Number(e.currentTarget.value));
      }}
      type="number"
      value={value}
    />
  );
}

function Circle({
  size,
  x,
  y,
  onMove
}: {
  size: number;
  x: number;
  y: number;
  onMove: (x: number, y: number) => void;
}) {
  const isMouseDown = React.useRef(false);

  const dragX = React.useRef(0);
  const dragY = React.useRef(0);

  React.useLayoutEffect(() => {
    const onMouseMove = (e: MouseEvent) => {
      if (isMouseDown.current) {
        if (dragX.current === 0 && dragY.current === 0) {
          dragX.current = e.pageX;
          dragY.current = e.pageY;
        }

        const deltaX = e.pageX - dragX.current;
        const deltaY = e.pageY - dragY.current;
        dragX.current = e.pageX;
        dragY.current = e.pageY;

        const nextX = x + deltaX;
        const nextY = y + deltaY;

        onMove(nextX, nextY);
      }
    };

    const onMouseUp = () => {
      isMouseDown.current = false;
      dragX.current = 0;
      dragY.current = 0;
    };

    // We listen to the mouse position of the window instead
    // of just the circle. This ensures we maintain the mouse
    // position if we drag really fast outside of the circle.
    window.addEventListener("mousemove", onMouseMove);
    window.addEventListener("mouseup", onMouseUp);

    return () => {
      window.removeEventListener("mousemove", onMouseMove);
      window.removeEventListener("mouseup", onMouseUp);
    };
  });

  return (
    <div
      onMouseDown={() => {
        isMouseDown.current = true;
      }}
      style={{
        width: size,
        height: size,
        position: "absolute",
        backgroundColor: "#ff705a",
        borderRadius: size / 2,
        display: "flex",
        flexDirection: "column",
        alignContent: "center",
        justifyContent: "center",
        transform: `translate(${x}px, ${y}px)`
      }}
    >
      <div>
        X{" "}
        <NumberInput
          value={Number(x.toFixed())}
          onChange={newX => onMove(newX, y)}
        />
      </div>
      <div>
        Y{" "}
        <NumberInput
          value={Number(y.toFixed())}
          onChange={newY => onMove(x, newY)}
        />
      </div>
    </div>
  );
}

function getDistance(
  fromX: number,
  fromY: number,
  toX: number,
  toY: number
): number {
  return Math.sqrt(Math.pow(toX - fromX, 2) + Math.pow(toY - fromY, 2));
}

function getRadian(
  fromX: number,
  fromY: number,
  toX: number,
  toY: number
): number {
  return Math.atan2(toY - fromY, toX - fromX);
}

function Line({
  fromX,
  fromY,
  toX,
  toY,
  onChange
}: {
  fromX: number;
  fromY: number;
  toX: number;
  toY: number;
  onChange: (length: number) => void;
}) {
  const width = Math.round(getDistance(fromX, fromY, toX, toY));
  const radian = getRadian(fromX, fromY, toX, toY);

  return (
    <div
      style={{
        width,
        position: "absolute",
        zIndex: -1,
        background: "#001733",
        height: 2,
        transformOrigin: "0 0",
        transform: `translate(${fromX}px, ${fromY}px) rotate(${radian}rad)`,
        display: "flex",
        justifyContent: "center"
      }}
    >
      <NumberInput
        value={Number(width.toFixed())}
        onChange={onChange}
        style={{
          background: "#001733",
          borderRadius: 10,
          padding: 10,
          color: "#FFF",
          marginTop: -20,
          height: 20
        }}
      />
    </div>
  );
}

type CirclePos = [number, number];
type State = CirclePos[];

type Action =
  | {
      type: "move";
      index: number;
      x: number;
      y: number;
    }
  | {
      type: "adjust-line";
      toIndex: number;
      length: number;
    };

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "move":
      return state.map((pos, i) => {
        if (i === action.index) {
          return [action.x, action.y];
        }
        return pos;
      });
    case "adjust-line":
      return state.map((pos, i) => {
        const prevCircle = state[i - 1];
        if (i === action.toIndex && prevCircle) {
          const radian = getRadian(
            prevCircle[0],
            prevCircle[1],
            pos[0],
            pos[1]
          );
          const nextX = action.length * Math.cos(radian);
          const nextY = action.length * Math.sin(radian);
          return [nextX, nextY];
        }
        return pos;
      });
  }
}

const App: React.FC = () => {
  const [state, dispatch] = React.useReducer(reducer, [[0, 0], [401, 290]]);

  return (
    <div
      style={{
        textAlign: "center"
      }}
    >
      {state.map(([x, y], i) => {
        let toXY;
        if (state[i + 1]) {
          toXY = state[i + 1];
        }
        return (
          <React.Fragment key={i}>
            <Circle
              key={i}
              size={150}
              x={x}
              y={y}
              onMove={(x, y) => {
                dispatch({ type: "move", index: i, x, y });
              }}
            />
            {toXY && (
              <Line
                fromX={x + 150 / 2}
                fromY={y + 150 / 2}
                toX={toXY[0] + 150 / 2}
                toY={toXY[1] + 150 / 2}
                onChange={length => {
                  dispatch({
                    type: "adjust-line",
                    toIndex: i + 1,
                    length: length
                  });
                }}
              />
            )}
          </React.Fragment>
        );
      })}
    </div>
  );
};

export default App;
